#!/usr/bin/env python
from collections.abc import Callable
import numpy as np
import pandas as pd
import os, sys, pickle, uproot, csv, string, operator 
from tqdm import tqdm
import matplotlib.pyplot as plt
sys.path.append("xsec-calc/")
try:
	from CrossSection import CrossSection
except ModuleNotFoundError:
	print('CrossSection module not found - did you include --recursive when cloning repo?')
from TruthRecoPlot import TruthRecoPlot
from atlasify import atlasify
from matplotlib.ticker import NullFormatter, MaxNLocator

def get_weights(truth_df, reco_df, custom_xsec=-1, reco_weight_key='', DSID_key='', DSID=-1):
	'''
	Calculate histogram weights for dataframes of reco/truth ntuples

	truth_df: Dataframe containing truth ntuple 
	reco_df: Dataframe containing reco ntuple 
	custom_xsec: either a single value for the xsec or a callable function to calculate the xsec from truth_df 
	DSID_key: Column in reco_df containing DSID (used to retrieve cross section from PMG database)
	DSID: DSID for this point (used to retrieve cross section from PMG database)
        reco_weight_key: the name of the branch containing histogram weights for reco ntuple
	'''	
	# set event weights reco
	if reco_weight_key!='':
		reco_df = reco_df.assign(hist_weight=reco_df[reco_weight_key])
	elif 'eventWeight' in reco_df.columns:
		reco_df = reco_df.assign(hist_weight=reco_df['eventWeight'])
	else:
		try: 
			reco_df['hist_weight'] = 139000*reco_df['WeightEvents']*reco_df['xsec']*reco_df['WeightLumi']*reco_df['WeightEventsPU']*reco_df['WeightEventselSF']*reco_df['WeightEventsmuSF']*reco_df['WeightEventsJVT']*reco_df['WeightEventsbTag']*reco_df['WeightEvents_trigger_single']
		except KeyError:
			print("ERROR: Couldn't find event weight info for reco trees")
			print("Please specify the name of the branch with histogram weights via reco_weight_key") 
			sys.exit(1)

	# Get cross section info
	if DSID_key in reco_df.columns:
		dsid = reco_df[DSID_key].iloc[0]
	elif DSID!=-1:
		dsid = DSID
	else:
		dsid=-1

	if dsid!=-1:
		xsecDB = CrossSection()
		xsec = xsecDB.xsecTimesEffTimeskFac(str(dsid))
		tqdm.write(f'Xsec for DSID {dsid} set from PMG database')
	elif isinstance(custom_xsec, Callable):
		xsec = custom_xsec(truth_df)
	elif custom_xsec!=-1:
		xsec = custom_xsec	
	else:
		print('ERROR: Xsec could not be set. Did you set either DSID_key, DSID_dict or custom_xsec?')
		sys.exit(1)
	F = xsec*139000
	# set event weights for truth
	truth_df = truth_df.assign( hist_weight = F*truth_df['eventWeight']/np.sum(truth_df['eventWeight']) )
	return truth_df, reco_df


def preselection(df, selection_list, writeCutflow):
	'''
	Apply selections to a pandas dataframe

	df: pandas dataframe
	selection_list: list of selections to apply, formatted like ['lep1pT>10','isOS==1',...]
	writeCutflow: if non-empty string, cutflows will be saved here
	'''
	txt_file = open(writeCutflow,'w')
	txt_file.write('cut, nRawEvents, nWeightedEvents\n')
	txt_file.write('NoCut,'+str(len(df))+','+str(np.sum(df['hist_weight']))+'\n')
	for _sel in selection_list:
		_sel = _sel.replace(" ","") 
		if '==' in _sel:
			_var = _sel.split('==')[0]
			_val = int(_sel.split('==')[1])
			if _var not in df.columns:
				print('WARNING:',_var,'not found, preselection will not be applied!')
				continue
			df = df[df[_var]==_val]
		elif '<' in _sel:
			if '>' in _sel:
				raise ValueError('Selection '+_sel+'contains more than one operator')
			if'<=' in _sel:
				_var = _sel.split('<=')[0]	
				_val = float(_sel.split('<=')[1])	
				if _var not in df.columns:
					print('WARNING:',_var,'not found, preselection will not be applied!')
					continue

				df = df[df[_var]<=_val]
			else:
				_var = _sel.split('<')[0]	
				_val = float(_sel.split('<')[1])
				if _var not in df.columns:
					print('WARNING:',_var,'not found, preselection will not be applied!')
					continue
				df = df[df[_var]<_val]
		elif '>' in _sel:
			if '<' in _sel:
				raise ValueError('Selection '+_sel+'contains more than one operator')
			if'>=' in _sel:
				_var = _sel.split('>=')[0]	
				_val = float(_sel.split('>=')[1])	
				if _var not in df.columns:
					print('WARNING:',_var,'not found, preselection will not be applied!')
					continue
				df = df[df[_var]>=_val]
			else:
				_var = _sel.split('>')[0]	
				_val = float(_sel.split('>')[1])
				if _var not in df.columns:
					print('WARNING:',_var,'not found, preselection will not be applied!')
					continue
				df = df[df[_var]>_val]
		else:
			raise ValueError('Selection '+_sel+'does not contain one of: ==, <, <=, >, >=')
		txt_file.write(_sel+','+str(len(df))+','+str(np.sum(df['hist_weight']))+'\n')
	txt_file.close()
				
	return df

def test_SR_definition(df,SR_definition, SR_names):
	'''
	Apply SR_definition to df and check it gives the same acceptance as SimpleAnalysis
	'''
	test_df = SR_definition(df)
	for SR in SR_names:
		if SR[:3]!='SR_' or SR[3:] not in df.columns:
			print('ERROR: SR name',SR,'not recognised -- does it have the format "SR_{SimpleAnalysis SR Name}"?')
			sys.exit(1)
		simple_ana_yield = np.sum(df[ SR[3:] ])
		user_yield = np.sum(test_df[ test_df[SR] ]['eventWeight'])
		if not np.isclose(simple_ana_yield, user_yield):
			print(f'WARNING: For {SR}, SR_definition yield is {user_yield}, but yield from SimpleAnalysis is {simple_ana_yield} - please check SR_definition is correct!')
	return test_df

def yield_correlation(yieldList, saveTo, log=True,ax_lim=(0.1,70), extra_label='SR yields'):
	''' 
	Make SR yield correlation plot 

	yieldList: [ [listOfTruthYields], [listOfTruthYieldUncertainties], [listOfRecoYields], [listOfRecoYieldUncertainties] ]
	saveTo: /path/to/save/plot/to/plotName.png
	log: Flag for log-scaled axes
	ax_lim: (lower_axis_bound, upper_axis_bound)
	extra_label: String with any label you want below the ATLAS badge on the plot
	'''
	yieldList = np.array(yieldList)
	x = yieldList[0]	
	xErr = yieldList[1]	
	y = yieldList[2]	
	yErr = yieldList[3]	
	fig, ax = plt.subplots()
	#ax.scatter(x=x,y=y)
	ax.errorbar(x=x,y=y,yerr=yErr,xerr=xErr, marker='o',ls='none',markersize=4,ecolor='black',elinewidth=1)
	ax.set_xlim(ax_lim[0],ax_lim[1])
	ax.set_ylim(ax_lim[0],ax_lim[1])
	ax.set_xlabel('Number of smeared-truth events')
	ax.set_ylabel('Number of reco events')
	ax.plot([ax_lim[0],ax_lim[1]],[ax_lim[0],ax_lim[1]],'--',color='black',linewidth=1)
	atlasify('Internal','$\\sqrt{s}=13$ TeV, 139 fb$^{-1}$\n'+extra_label,axes=ax, enlarge=1.0)
	if log==True:
		ax.set_yscale('log')
		ax.set_xscale('log')
	plt.savefig(saveTo)
	plt.close()


def make_plots(truth_dict={}, reco_ntuple='', plot_list=[{}], outPath='plots/', plt_format='png', var_name_map={}, 
		SA_key='EwkTwoLeptonZeroJet2018__ntuple', reco_weight_key = '', DSID_key = '', DSID_dict={}, custom_xsec={}, 
		extra_presel_truth=[], extra_presel_reco=[], SR_definition=None, corr_ax_min = 0.1, corr_ax_max=70, corr_log = True,):
	'''
	Make truth v reco distributions

	truth_dict: dictionary of {grid_point : pathToRootFile}
	reco_ntuple: /path/to/reco/trees.root
	plot_list: list of dictionaries with details for each variable to plot (see example.py for an example)
	outPath: directory to save plots to
	plt_format: 'png' or 'pdf'
	custom_xsec: - Optional dictionary of cross-sections for each point. 
	             - By default Xsecs are found in PMG database using DSIDs, so this argument only needs to be used for 
		     points not in the PMG database or if the DSID is not provided in the ntuples
	DSID_key: Name of variable in either reco or truth ntuple (both will be checked) containing DSID
	reco_weight_key: Name of variable in reco tree containing histogram weights
	var_name_map: - dictionary of variables to be renamed in either truth/reco ntuples 
                      - needed if variables have different names in each ntuple
        SA_key: name of tree in truth ntuples (probably '{analysisName}__ntuple'
        extra_presel_truth: list of preselections to apply to truth ntuple, formatted like ['lep1pT>10','isOS==1',...]
        extra_presel_reco: list of preselections to apply to reco ntuple, formatted like ['lep1pT>10','isOS==1',...]
	SR_definition: function which takes a pandas dataframe and adds columns 'SR_{SR_name}' with boolean for each SR
	corr_axmin, corr_axmax : axis range for yield correlation plot
	'''
	# set-up output directory
	if outPath[-1]!='/':
		outPath+='/'
	os.makedirs(outPath,exist_ok=True)

	# lists to record SR yields for each point for correlation plot
	truth_yields =[] 
	truth_yields_err = []
	reco_yields = []
	reco_yields_err = []

	for point in tqdm(truth_dict, desc='Plotting for each grid point'):
		tqdm.write(point)
		os.makedirs(outPath+point,exist_ok=True)

		# open truth ntuple
		try:
			df = uproot.open( truth_dict[point])
		except FileNotFoundError:
			print('Truth ntuple not found for point',point,'-- Skipping.')
			continue
		try:
			df = df[SA_key]
			print('Opened tree',SA_key,'in',truth_dict[point])
		except KeyError:
			keys = df.keys()
			if len(keys)==1:
				print('WARNING: SA_key=',SA_key,'not recognised. Guessing tree name',keys[0],'...')
				df = df[keys[0]]
				print('Opened tree',keys[0],'in',truth_dict[point])
			else:
				print('SimpleAnalysis ntuple treename =',SA_key,'not recognised')
				print('Did you mean one of these:')
				print(keys)
				sys.exit(1)
		df = df.arrays(outputtype=pd.DataFrame)
		df = df.rename(var_name_map,axis=1)
		# open reco ntuple
		if type(reco_ntuple) is str:
			reco_df = uproot.open(reco_ntuple)
		elif type(reco_ntuple) is dict:
			reco_df = uproot.open(reco_ntuple[point])
		else:
			print('ERROR: reco_ntuple must be dict or str')
			sys.exit(1)
		reco_keys = [s.decode('utf-8') for s in reco_df.keys()]
		reco_point_key=''
		for key in reco_keys:
			if point in key:
				reco_point_key = key
		if reco_point_key=='':
			if len(reco_keys)==1:
				reco_point_key = reco_keys[0]
				print('Looking at tree',reco_point_key,'in reco file for point',point,' -- is that right?')
			else:
				print('WARNING: Unable to identify reco tree for point',point)
				print('Make sure',point,'matches the reco tree names! Skipping...')
		try:
			reco_df = reco_df[reco_point_key]
		except KeyError:
			print('Reco ntuple named',reco_point_key,'not found. Skipping this point...')
			continue
		reco_df = reco_df.arrays(outputtype=pd.DataFrame)
		reco_df = reco_df.rename(var_name_map,axis=1)

		# Check plot variables exist and convert units 
		checked_plot_list = []
		for v in plot_list:
			v['title'] = point
			name = v['name']
			unit = v['unit']
			if name not in reco_df.columns:
				print('WARNING:',name,'not found in reco ntuple for point',point,'. This plot will be skipped. If this variable is in your SR_definition or extra_presel, things will break...')
				continue
				
			if name not in df.columns:
				print('WARNING:',name,'not found in truth ntuple for point',point,'. This plot will be skipped. If this variable is in your SR_definition or extra_presel, things will break...')
				continue

			# convert MeV to GeV if needed:
			if name in reco_df.columns and 'recoMeV2GeV' in v and v['recoMeV2GeV']==True:
				reco_df[name] = reco_df[name]/1000.0
			if name in df.columns and 'truthMeV2GeV' in v and v['truthMeV2GeV']==True:
				df[name] = df[name]/1000.0

			checked_plot_list.append(v)

		if point in custom_xsec:
			_xsec = custom_xsec[point]
		else:
			_xsec = -1
		if point in DSID_dict:
			_dsid = DSID_dict[point]
		else:
			_dsid = -1
		# get histogram weights
		df, reco_df = get_weights(df, reco_df,reco_weight_key = reco_weight_key, custom_xsec=_xsec, DSID_key=DSID_key, DSID=_dsid)

		# preselections
		df = preselection(df, extra_presel_truth, outPath+point+'/Truth_cutflow.txt')
		reco_df = preselection(reco_df, extra_presel_reco, outPath+point+'/Reco_cutflow.txt')

		# record SR yields
		if SR_definition is not None:
			# apply SR selections to reco
			reco_df = SR_definition(reco_df)
			SR_names = [c for c in reco_df.columns if c[:3]=='SR_']
			if len(SR_names)==0:
				print('WARNING: No SR names found - did you start column names with "SR_" in SR_definition()? Exiting...')
				sys.exit(1)

			# apply SR selections to truth and check they give the same result as SimpleAnalysis 
			df = test_SR_definition(df, SR_definition, SR_names)
			
			for SR in SR_names:
				truth_yields.append(np.sum(df[ df[SR] ]['hist_weight']))
				truth_yields_err.append( np.sqrt( np.sum(df[ df[ SR ] ]['hist_weight']**2) ) )
				reco_yields.append(np.sum(reco_df[ reco_df[ SR ] ]['hist_weight']))
				reco_yields_err.append( np.sqrt( np.sum( reco_df[ reco_df[ SR ] ]['hist_weight']**2) ) )

		df_dict = {}
		df_dict['Truth'] = df
		df_dict['Reco'] = reco_df

		# make kinematic distributions
		x = TruthRecoPlot( outPath+point, [df_dict['Reco'],df_dict['Truth']], label=['Reco','Truth'], plt_format=plt_format)
		x.plot_from_dict(checked_plot_list)

	# make yield correlation plot
	if len(truth_yields)>0:
		yield_correlation([truth_yields, truth_yields_err, reco_yields, reco_yields_err], outPath+'SR_yield_correlation.png', log=corr_log, ax_lim=(corr_ax_min, corr_ax_max))

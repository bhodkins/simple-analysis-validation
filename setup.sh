#!/bin/bash

if [ -d "SA-valid-env" ]; then
  source SA-valid-env/bin/activate
else
  python3 -m venv SA-valid-env
  source SA-valid-env/bin/activate
  pip install --upgrade pip
  pip install -r requirements.txt
fi

# Simple analysis validation

Scripts for creating Simple Analysis validation plots comparing distributions/yields for (smeared-)truth and reco.

## Set-up environment

First-time setup:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/bhodkins/simple-analysis-validation.git
cd simple-analysis-validation/
source setup.sh
```
In future just run `source setup.sh` to set-up.

## Example plots

`mkdir test_inputs`

Download example files for 2L0J from https://cernbox.cern.ch/index.php/s/YgD5WaA0R1oR8en and put in `test_inputs`

`./example.py`

## Make your validation plots

Go through `example.py` and modify with your own input file locations, variable names, SR definitions etc.

`./example.py` to run

#### Note:
These scripts were written for and tested with the 2L0J analysis, running on lxplus. They've also been tested with 2L2J and 4L, but I can't guarantee they'll work perfectly with other analyses - email ben.hodkinson@cern.ch if you have issues!

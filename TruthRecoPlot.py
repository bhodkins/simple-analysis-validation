import matplotlib.pyplot as plt
from atlasify import atlasify
import matplotlib as mpl
import matplotlib.ticker as ticker
import numpy as np
from math import log10, floor, ceil
import os

class TruthRecoPlot:
	"""
	A class for making ATLAS style truth v reco (or any other comparison) plots with ratio.
	"""
	
	def __init__(
		self,
		out_dir,
		listOfDF,
		label,
		listOfWeight = [],
		color = [],
		atlasify_label="$\\sqrt{s}=13$ TeV, 139.0 fb$^{-1}$\n",
		plt_format='png',
	): 
		"""
		out_dir: directory to save plots to
		listOfDF: list of pandas dataframes containing variables to be plotted. 
			  The first in the list is used for the ratio plot.
		labels: label for each dataframe.
		listOfWeight: list of numpy arrays containing sample weights. 
			      If left empty, dataframes must contain columns titled "hist_weight".
		color: color for each DF
		"""
		self.out_dir = out_dir
		if not os.path.exists(out_dir):
			os.makedirs(out_dir)
		self.listOfDF = listOfDF
		self.label = label
		self.plt_format=plt_format

		self.ax_ratio_lines = [0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5]
		self.atlasify_label = atlasify_label
		if len(self.label)!=len(self.listOfDF):
			print('WARNING: label length must match listOfDF')
		if len(listOfWeight) == 0:
			self.listOfWeight = [df['hist_weight'].values for df in listOfDF]
		elif len(listOfWeight)==len(listOfDF):
			self.listOfWeight = listOfWeight
		else:
			print('WARNING: listOfWeight length must match listOfDF')
		if len(color)==0:
			self.color = ['C'+str(n) for n in range(0,len(listOfDF))]
		elif len(color)==len(listOfDF):
			self.color = color
		else:
			print('WARNING: color length must match listOfDF')

	def round_to_1(self, x):
		"""
		Function to round x to 1 s.f
		"""
		return round(x, -int(floor(log10(abs(x)))))

	def plot(self, var,  xlabel = '', xticks = 'default', xtick_rotate = 0, x_range = (0,10),
		unit = '', bins = 10, log = True, title = '', norm=False):
		"""
		var : dataframe key for variable to plot
		xlabel : label for x axis
		xticks : list of non-numerical xticklabels ('default' uses numerical ticks with x_range)
		xtick_rotate : rotation angle for x ticks (only if non-numerical)
		x_range : range of x axis
		unit : unit of variable
		bins : number of x axis histogram bins
		log : True/False flag for log scale y axis
		title : plot title
		norm : True/False flag for normalizing histograms
		"""
		# make plot with main axis and ratio axis
		fig, (ax, ax_ratio) = plt.subplots(2,1, gridspec_kw={'height_ratios':[3,1]},sharex=True)
		ax_ratio.set_xlabel( xlabel + ' '+unit )
		h = ax_ratio.set_ylabel('Ratio / '+self.label[0])
		fig.subplots_adjust(hspace=0.1)
		#plt.setp(ax.get_xticklabels(),visible=False)
		ax_ratio_lines = self.ax_ratio_lines
		for y in ax_ratio_lines:
			ax_ratio.axhline(y=y,color='black',linewidth=0.25, linestyle='--') #horizontal lines on ratio plot to guide eye
		ax_ratio.axhline(y=ax_ratio_lines[int(len(ax_ratio_lines)/2)],color='black',linewidth=0.75)

		# plot histogram
		n, bin_pos , _ = ax.hist(
			[df[var] for df in self.listOfDF],
			 label=self.label,
			 weights = self.listOfWeight,
			 color=self.color,
			 bins=bins, range=x_range, density=norm, histtype='step', 
			 log=log,
		)
		mid = 0.5*(bin_pos[1:]+bin_pos[:-1])
		bin_width = bin_pos[1]-bin_pos[0]
		with np.errstate(divide='ignore',invalid='ignore'):
			# error bars
			err_list = []
			for i in range(0, len(self.listOfDF)):
				if norm==True:
					continue
				df = self.listOfDF[i]
				weight = self.listOfWeight[i]
				err = np.sqrt( np.histogram( df[var], bins=bins, range=x_range, weights=weight**2)[0] )
				err_list.append(err)
				ax.errorbar(mid, n[i], yerr = err, fmt='o', markersize=1, color=self.color[i])
			# plot ratios
			for i in range(1,len(self.listOfDF)):
				if norm==True:
					ax_ratio.plot(mid, np.array(n[i])/np.array(n[0]), 'o', color=self.color[i], markersize=3)
				else:
					ratio_err = ( np.array( n[i] )/np.array( n[0] ) )*np.sqrt( (err_list[i]/n[i])**2 + ( err_list[0]/n[0] )**2 )
					ax_ratio.errorbar(mid, np.array(n[i])/np.array(n[0]), yerr = ratio_err, fmt='o', color=self.color[i], markersize=3)
		ax_ratio.set_ylim(ax_ratio_lines[0]-0.05,ax_ratio_lines[-1]+0.05)
		bin_width=self.round_to_1(bin_width)
		# set y label
		if bin_width%1==0:
			bin_width=int(bin_width)	
		if unit == '':
			ax.set_ylabel('Entries')
		else:
			ax.set_ylabel('Entries / '+str(bin_width)+ ' ' + unit)
		lgd = ax.legend(loc='upper right',ncol=ceil(len(self.listOfDF)/3))
		ax.set_title(title)
		atlasify("Internal",self.atlasify_label, axes=ax)
		# set y axis scale
		if log:
			ax.yaxis.set_minor_locator(mpl.ticker.LogLocator(base=10.0, 
			subs=np.arange(1.0,10.0)*0.1, numticks=10))
		# customise x ticks
		if xticks!='default':
			ticks = [n for n in range(x_range[0], x_range[1]+1)]
			lab_loc = [n+0.5 for n in range(x_range[0],x_range[1])]
			ax.set_xticks(ticks)
			ax_ratio.set_xticks(ticks)
			ax.tick_params(axis='x',which='minor',bottom=False)
			ax_ratio.tick_params(axis='x',which='minor',bottom=False)
			ax_ratio.xaxis.set_major_formatter(ticker.NullFormatter())
			ax_ratio.xaxis.set_minor_locator(ticker.FixedLocator(lab_loc))
			ax_ratio.xaxis.set_minor_formatter(ticker.FixedFormatter(xticks))
			plt.setp(ax_ratio.xaxis.get_minorticklabels(), rotation = xtick_rotate)
		# set y range
		ymax_list = np.array([np.amax(n_i) for n_i in n])
		ymax = np.amax(ymax_list)*1.5 
		if ymax==0:
			ymax=1
		ymin_list = np.array([np.amin(np.array(n_i)[np.array(n_i)>0]) for n_i in n])
		if len(ymin_list)>0:
			ymin = np.amin(ymin_list)*0.9
			if ymin>1:
				ymin=int(ymin)
		else:
			ymin=0
			if log:
				ymin=0.1
		if log==False:
			ax.set_ylim(0,ymax)
		else:
			up = 10**(round(log10(ymax))+1)	
			ax.set_ylim(ymin, up ) 
		plt.tight_layout()
		plt.savefig(self.out_dir + '/' + var + '.'+self.plt_format)
		plt.close()

	def plot_from_dict(self, var_dict):
		"""
		Make plot using var_dict
		"""
		default_dict = {'xlabel':'', 'xticks':'default','xtick_rotate':0,
			'x_range':(0,10), 'unit':'', 'bins':10, 'log':True, 'title':'','norm':False} 
		for V in var_dict:
			if 'name' not in V:
				print('Error: Variable name must be specified in dictionary as "name"')
				return 0
			# check dict contains all necessary keys
			if 'xlabel' not in V:
				V['xlabel'] = V['name']
			for key in default_dict:
				if key not in V:
					V[key] = default_dict[key]
			self.plot( V['name'], V['xlabel'], V['xticks'], V['xtick_rotate'],
				V['x_range'], V['unit'], V['bins'], V['log'], V['title'], V['norm'])

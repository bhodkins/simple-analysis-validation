#!/usr/bin/env python

# Example script for making SimpleAnalysis validation plots
from SimpleAnaValidationPlots import *

# ===================================================================================================
# Set paths to inputs and outputs
# ===================================================================================================

# Path to save plots:
outPath = 'plots/'

# set output plot format
plt_format = 'png' #'pdf'

# Dictionary with path to reco ntuple for each signal point
#reco_ntuple = {'C1C1_WW_300_50': 'test_inputs/Signal.root', 'C1C1_WW_500_10',:'test_inputs/otherSignal.root' }
# If all the signal trees are in a single root file, you can instead specify the path to this root file:
reco_ntuple = 'test_inputs/Signal.root'

# Dictionary with path to truth(-smeared) ntuple for each signal point
# - the ntuples should have been produced with SimpleAnalysis with the -n flag
truth_dict = {
	'C1C1_WW_300_50': 'test_inputs/C1C1_WW_300_50.root',
	'C1C1_WW_500_10': 'test_inputs/C1C1_WW_500_10.root',
	}

# Name of tree containing kinematic variables in simple analysis output files (probably '{AnalysisName}__ntuple')
SA_key = 'EwkTwoLeptonZeroJet2018__ntuple'

# ===================================================================================================
# Provide information required for cross section calculation
# ===================================================================================================

# Name of variable in reco tree containing histogram weights
# Note if your ntuple doesn't have histogram weight info, you must instead modify L27-37 of
# SimpleAnaValidationPlots.py to calculate the histogram weight
reco_weight_key = 'eventWeight'


# Three ways for the plotting code to get cross sections in order to calculate histogram weights for truth ntuples:
# Note: Whichever of the 3 you use, leave the other 2 as empty strings/dictionaries

# 1) Provide the DSID for each point in a dictionary. Cross sections will be grabbed from the PMG database using this:
#DSID_dict = {'C1C1_WW_300_50':100001}
DSID_dict={}

# 2) If the DSID for each point is provided as a variable in the reco ntuple, specify this variable name via DSID_key. Again, cross sections will be grabbed from the PMG database using this:
DSID_key = 'DSID'
#DSID_key=''

# 3) Provide the cross section in ab-1 for each point in a dictionary :
#custom_xsec = {'C1C1_WW_300_50' : 0.001} 
custom_xsec = {} 
# Note: You can also provide a function to calculate the cross section from columns in your truth ntuple. See this example from 2L2J:
#custom_xsec = {
#    "C1N2N1_GGMHinoZh50_200": 1.33562 * 1.6333e-01 * 1.0,
#    "C1N2_WZ_250p0_100p0_2L2J": 0.053410997646 * 0.615160 * 1.0,
#    # off-shell are split into C1p and C1m by dsid
#    "C1N2_WZ_100p0_60p0": lambda df: np.where(
#        df["mcDSID"] == 377411,
#        0.28046552466 * 2.5241e-01 * 1.0,
#        0.172891654332 * 2.5433e-01 * 1.0,
#    ),
#}


# ===================================================================================================
# Define preselections
# ===================================================================================================

# - define preselections to apply 
# - might be needed if presel for reco trees and simple analysis ntuples are different)
# - this will also produce a txt file with a cutflow for each point
# - each selection must have the format '{variableName}{== OR > OR < OR >= OR <=}{number}'
extra_presel_truth = [ # presel for truth ntuples
	'nLeps==2',
	'lept1Pt>25',
	'lept2Pt>25',
	'L2MT2>60',
	'L2Mll>=25',
] 
extra_presel_reco = [ # presel for reco ntuples
	'nLeps==2',
	'lept1Pt>25',
	'lept2Pt>25',
	'L2MT2>60',
	'L2Mll>=25',
]

# ===================================================================================================
# Set-up for plotting distributions of kinematic variables
# ===================================================================================================

# If any variables have different names in the reco and truth ntuples, map them with this dictionary:
var_name_map = {'nlep':'nLeps', 'njet':'L2nCentralLightJet'}

# Set kinematic variables to plot via a list of dictionaries
# Each dictionary specifies the plot options for each kinematic variable: 
# - 'name' : name of the variable in the root file (must be specified)
# - 'xlabel' : x axis label (excluding unit)
# - 'x_range' : x axis range
# - 'bins' : number of histogram bins
# - 'unit' : '[rad]'/'[GeV]'/'' -- unit added to x and y axis labels
# - 'log' : True/False for plotting log y axis  (default = True)
# - 'norm' : True/False for normalizing distributions  (default = False)
# - recoMeV2GeV, truthMeV2GeV: Set True to convert this variable from MeV to GeV for reco/truth (needed if units are different for reco and truth ntuples)

plot_list = [
	{'name': "lept1Pt",            'xlabel': 'lep1pT',            'x_range': (0,550),       'bins': 10, 'unit': '[GeV]', 'log':True, 'norm':False, 'recoMeV2GeV':True},
	{'name': "lept1Eta",           'xlabel': 'lep1Eta',           'x_range': (-3,3),        'bins': 15, 'unit': '',      'log':True, 'norm':False},
	{'name': "lept1Phi",           'xlabel': 'lep1Phi',           'x_range': (-3.5,3.5),    'bins': 15, 'unit': '[rad]', 'log':True, 'norm':False},
	{'name': "lept2Pt",            'xlabel': 'lep2pT',            'x_range': (0,300),       'bins': 30, 'unit': '[GeV]', 'log':True, 'norm':False, 'recoMeV2GeV':True},
	{'name': "lept2Eta",           'xlabel': 'lep2Eta',           'x_range': (-3,3) ,       'bins': 15, 'unit': '',      'log':True, 'norm':False},
	{'name': "lept2Phi",           'xlabel': 'lep2Phi',           'x_range': (-3.5,3.5),    'bins': 15, 'unit': '[rad]', 'log':True, 'norm':False},
	{'name': "L2nCentralBJet",     'xlabel': 'Num bjets',         'x_range': (0,10) ,       'bins': 10, 'unit': '',      'log':True, 'norm':False},
	{'name': "L2nCentralLightJet", 'xlabel': 'Num light jets',    'x_range': (0,10) , 	'bins': 10, 'unit': '',      'log':True, 'norm':False}, 
	{'name': "MET",                'xlabel': 'MET',               'x_range': (0,550), 	'bins': 10, 'unit': '[GeV]', 'log':True, 'norm':False, 'recoMeV2GeV':True},
	{'name': "METsig",             'xlabel': 'MET Significance',  'x_range': (0,30) , 	'bins': 30, 'unit': '',      'log':True, 'norm':False},
	{'name': "L2Mll",              'xlabel': '$M_{\\ell\\ell}$',  'x_range': (0,1000),      'bins': 20, 'unit': '[GeV]', 'log':True, 'norm':False, 'recoMeV2GeV':True},
	{'name': "L2MT2",              'xlabel': 'MT2',               'x_range': (0,300),       'bins': 15, 'unit': '[GeV]', 'log':True, 'norm':False, 'recoMeV2GeV':True},
	]

# ===================================================================================================
# Set-up for 2D correlation plots of SR yields
# ===================================================================================================

#set axis range for SR yield correlation plot
corr_axis_min = 0.1
corr_axis_max = 10

# log axis for correlation plot?
corr_log = True

# To plot 2D correlation of signal region yields, you need to define the signal regions. 
# The function below takes a pandas dataframe and creates columns called "SR_{SimpleAnalysis_SR_name}" containing a boolean vector indicating if each row passes the selections for that given SR.
# Please adapt this function for your own SR definition, making sure the column name for each signal region is 'SR_'+'name of this SR in SimpleAnalysis'
# Using boolean vectors in pandas works the same as a NumPy array. See here for documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#boolean-indexing
# If you want, you could also adapt this function to print/write a cutflow for each selection
# NOTE: If your ntuples already contain boolean variables with SR acceptance, you can just copy this to a new column eg. df['SR_SR1A'] = df['SR1A'].astype(bool) 
def SR_definition(df):
	# Example SR definitions for 2L0J:

	# Boolean vectors indicating DF or SF leptons
	DF = np.abs(df['lept1Flav'])!=np.abs(df['lept2Flav'])
	SF = ~DF

	# Boolean vector of general signal region selections
	SR_selections = (df['L2nCentralBJet']==0) & (df['L2nCentralLightJet']<2) &\
		(df['MET']>110) & (df['METsig'] > 10) & (df['L2Mll']>100) &\
		(df['L2MT2']>100)

	# Boolean vectors of DF and SF signal region selections
	SR_DF = (DF) & (SR_selections)
	SR_SF = (SF) & (SR_selections) & (np.abs(df['L2Mll']-91.1876)>30)

	SR_DF0J = (SR_DF) & (df['L2nCentralLightJet']==0)
	SR_DF1J = (SR_DF) & (df['L2nCentralLightJet']==1)
	SR_SF0J = (SR_SF) & (df['L2nCentralLightJet']==0)
	SR_SF1J = (SR_SF) & (df['L2nCentralLightJet']==1)

	# Finally, add columns to dataframe with boolean vector for each indiivdual signal region
	df['SR_DF_0J_a'] = (SR_DF0J) & (df['L2MT2']>100) & (df['L2MT2']<105)
	df['SR_DF_1J_a'] = (SR_DF1J) & (df['L2MT2']>100) & (df['L2MT2']<105)
	df['SR_SF_0J_a'] = (SR_SF0J) & (df['L2MT2']>100) & (df['L2MT2']<105)
	df['SR_SF_1J_a'] = (SR_SF1J) & (df['L2MT2']>100) & (df['L2MT2']<105)

	return df

# ===================================================================================================
# Run plotting scripts...
make_plots(
	truth_dict, 
	reco_ntuple, 
	plot_list, 
	outPath, 
	plt_format = plt_format, 
	var_name_map = var_name_map, 
	SA_key = SA_key, 
	extra_presel_truth = extra_presel_truth, 
	extra_presel_reco = extra_presel_reco, 
	SR_definition=SR_definition, 
	corr_ax_min = corr_axis_min, 
	corr_ax_max = corr_axis_max, 
	corr_log = corr_log, 
	reco_weight_key=reco_weight_key, 
	DSID_key = DSID_key, 
	DSID_dict=DSID_dict, 
	custom_xsec=custom_xsec,
)
